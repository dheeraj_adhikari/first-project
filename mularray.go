package main

import "fmt"

func main() {
	var n = [][]int{{1, 2}, {3, 4}, {5, 7}}
	var i int
	var j int
	for i = 0; i < 3; i++ {
		for j = 0; j < 2; j++ {
			fmt.Println("a[", i, "] [", j, "] =", n[i][j])
		}
	}
}
